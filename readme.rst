Gestion de la aplicacion del recetario en PHP:
=======================================

Esta gestión esta echa con PHP, podremos gestionar un recetario.

Utilizaremos la hoja de estilo, las imagenes y base de datos que ya usemos en el anterior recetario.

Explicación para usar la aplicación:
=====================================
-Ponemos el proyecto en un servidor web. En este caso local.
-Instalamos Composer en nuestra maquina desde la terminal.
-Instalamos ($php composer.phar install).
-Y luego ya podemos probar la aplicación.
-Si da algun problema de permisos (le otorgamos todos los permisos a la carpeta que contiene el proyecto).

Explicación para usar la aplicación:
=====================================

Para navegar en la aplicación usaremos un menu superior horizontal, que esta dividido en:

	-Inicio: Aqui veremos las ultimas recetas registradas.
	
	-Usuarios: Apareceran los usuarios registrados en aplicación, con sus respectivas recetas.
	
	-Recetas: Podremos ver todas las recetas.Si pinchamos sobre el titulo de cada receta podremos ver:
	
		-Ingredientes
		
		-Preparación
		
		-Comentarios 
	
	-Login: Aqui podras loguearte en la aplicacion. Si estas logeado podras realizar unas funciones, que son:
	
		-Agregar recetas.
		
		-Ver solo tus recetas.
		
		-Eliminar solo tus recetas.
		
		-Agregar comentarios a tus recetas.
		
		-Salir de la aplicación. (Logout)
	
	-Registrar usuario: Aqui nos saldra un formulario para poder registrar al usuario deseado.
