<?php
 $conn = neW PDO('sqlite:../db.sqlite3');
?>
<!DOCTYPE html>
<html lang='es'>
<head>
  <meta charset='utf-8'>
  <title>Recetario en PHP</title>
  <link rel="stylesheet"  href="../css/styles.css" />
</head>

<body leftmargin="15">
<div id="outline">

	<div id="header">
	
	<img src="../img/logo.png" alt="logo"> 
	</div> 

	<div id="navbar">
		<ul class="menu">
		 	<li><a href='inicio.php'>Inicio</a></li> 
		 	<li><a href='agregarreceta.php'>Agregar recetas</a></li>
		 	<li><a href='misrecetas.php'>Mis recetas</a></li>
		  	<!--<li><a href="">Comenta Receta</a></li>-->
		  	<li><a href='salir.php'>Salir</a></li> 
		</ul>  
	</div> 
	<h1>Ingresar en la aplicacion</h1>
<div id="content"> 
<table border="0" align="center">
<tr><td>
	<ul>
	 <h2>Registro de usuario:</h2><br/> <br/>
	 <?php 
	$titulo = $_POST['titulo'];
	$ingredientes = $_POST['ingredientes'];
	$preparacion = $_POST['preparacion'];
	$imagen = $_POST['imagen'];
	
	
	if ($titulo == "" || $ingredientes == "" || $preparacion == "" || $imagen== ""){
	echo "Se ha dejado algun campo sin rellenar";
	}
	else {
	//insertar datos
	//1.preparar sentencia de insercion
	$insertar = "INSERT INTO  principal_receta (titulo, ingredientes, preparacion, imagen) VALUES(:titulo, :ingredientes, :preparacion, :imagen)";
	$sentencia= $conn->prepare($insertar);
	$sentencia->BindValue(':titulo', $titulo);
	$sentencia->BindValue(':ingredientes', $ingredientes);
	$sentencia->BindValue(':preparacion', $preparacion);
	$sentencia->BindValue(':imagen', $imagen);
	$sentencia->execute();	
	

	echo"Los datos se han introducido correctamente";
	
	}
	?>
	</ul>

</td>
</tr>
</table>
</div> 
<div id="pie">
<p>Adrian Perez Cros</p>
</div>
</div>
</body>
</html>