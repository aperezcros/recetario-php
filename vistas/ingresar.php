<?php
 $conn = neW PDO('sqlite:../db.sqlite3');
?>
<!DOCTYPE html>
<html lang='es'>
<head>
  <meta charset='utf-8'>
  <title>Recetario en PHP</title>
  <link rel="stylesheet"  href="../css/styles.css" />
</head>

<body leftmargin="15">
<div id="outline">

	<div id="header">
	
	<img src="../img/logo.png" alt="logo"> 
	</div> 

	<div id="navbar">
		<ul class="menu">
		 	<li><a href='../index.php'>Inicio</a></li>
		 	<li><a href='usuarios.php'>Usuarios registrados</a></li>
		  	<li><a href='recetas.php'>Recetas</a></li>
      		<li><a href='ingresar.php'>Login</a></li>
      		<!--<li><a href='vistas/registrarusuario.php'>Registrar usuario</a></li>-->
		</ul>
	</div> 
	<h1>Ingresar en la aplicacion</h1>
<div id="content"> 
<table border="0" align="center">
<tr><td>
	<ul>
	 <h2>Ingrese sus datos:</h2><br/> <br/>
	  <form id='formulario' method='post' action='entrada.php'>
			 
			 <label>Usuario:
			 <input type="text" name="usuario"  size="30"> </label>
			 <br/> <br/>
			 <label>Contraseña:
			 <input type="password" name="contrasena"  size="30"> </label>
			 <br/> <br/>
		<p><input type='submit' value='Ingresar'/></p>
	</form>
	</ul>

</td>
</tr>
</table>
</div> 
<div id="pie">
<p>Adrian Perez Cros</p>
</div>
</div>
</body>
</html>