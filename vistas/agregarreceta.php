<?php
 $conn = neW PDO('sqlite:../db.sqlite3');
?>
<!DOCTYPE html>
<html lang='es'>
<head>
  <meta charset='utf-8'>
  <title>Recetario en PHP</title>
  <link rel="stylesheet"  href="../css/styles.css" /> 
</head>

<body leftmargin="15">
<div id="outline">

	<div id="header">
	
	<img src="../img/logo.png" alt="logo"> 
	</div> 

	<div id="navbar">
		<ul class="menu">
		 	<li><a href='inicio.php'>Inicio</a></li>
		 	<li><a href='agregarreceta.php'>Agregar recetas</a></li>
		 	<li><a href='misrecetas.php'>Mis recetas</a></li>
		  	<!--<li><a href="">Comenta Receta</a></li>-->
		  	<li><a href='salir.php'>Salir</a></li> 
		</ul>   
	</div> 
	<h1>Insertar recetas</h1>
<div id="content"> 
<table border="0" align="center">
<tr><td>
	<ul>
	 <h2>Añadir receta:</h2><br/> <br/>
	  <form class="tabla" method="POST" action="añadir2.php">
			<label >Titulo: <br />
			 <input type="text" name="titulo"  size="30"> </label>
			 <br /> <br />
			 <label >Ingredientes: <br />
			 <textarea  name="ingredientes" rows='10' cols='50'></textarea></label>
			 <br /> <br />
			 <label >Preparacion: <br />
			 <textarea  name="preparacion" rows='10' cols='50'> </textarea></label>
			 <br /> <br />
			 <label >Imagen: <br />
			 <input type="file" name="imagen"/>
			 <br /> <br />
			
			<input type="submit" value="Añadir Receta">
		</form>
	</ul>

</td>
</tr>
</table>
</div> 
<div id="pie">
<p>Adrian Perez Cros</p>
</div>
</div>
</body>
</html>