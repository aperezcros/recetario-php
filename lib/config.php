<?php

$root = "root";
$passwd_admin="root";

function config_twig(){
	require_once realpath(dirname(__FILE__) . '/../vendor/twig/twig/lib/Twig/Autoloader.php');
	Twig_Autoloader::register();
	
	$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__) . '/../vistas/'));
	
	$twig = new Twig_Environment($loader); //,  array('cache' => '/tmp/cachetwig'	,));
	
}

//Para incluir en las demas paginas la conexion: include "lib/config.php";
$conn = neW PDO('sqlite:db.sqlite3');
?>